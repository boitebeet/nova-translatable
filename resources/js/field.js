import IndexField from './components/IndexField'
import DetailField from './components/DetailField'
import FormField from './components/FormField'
import Toggle from './components/Toggle'

Nova.booting((app, store) => {
  app.component('index-nova-translatable', IndexField)
  app.component('detail-nova-translatable', DetailField)
  app.component('form-nova-translatable', FormField)

  app.component('detail-nova-translatable-tabs', Toggle)
  app.component('form-nova-translatable-tabs', Toggle)
})
