# Installation
```composer require boitebeet/nova-translatable```

To use tiny-mce(rich text), add env variable TINY_MCE_API_KEY to your .env file.

## Configuration
Translatable fields require the translatable-tabs components to be somewhere in the page.

#Solution 1 (global resource fields)
in App\Nova\Resource.php
```php
/**
 * Get the fields that are available for the given request.
 *
 * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
 * @return \Laravel\Nova\Fields\FieldCollection
 */
public function availableFields(NovaRequest $request)
{
    $method = $this->fieldsMethod($request);

    return FieldCollection::make(array_merge(array_values($this->filter($this->{$method}($request))), [TranslatableTabs::make(null)]));
}
```

#Solution 2 (manually add field to resources)
In your resource fields method:
```php
public function fields(Request $request)
{
    return [
        TranslatableTabs::make(null),

        // Rest of your fields...
    ];
}
                    
```