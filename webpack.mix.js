let mix = require('laravel-mix')
let path = require('path')

require('./nova.mix')

mix
  .setPublicPath('dist')
  .js('resources/js/field.js', 'js')
  .vue({ version: 3 })
  .nova('boitebeet/nova-translatable')

mix.copyDirectory('node_modules/tinymce/skins', 'dist/tinymce/skins');
