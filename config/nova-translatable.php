<?php

return [
    /*
     * If a translation has not been set for a given locale, use this locale instead.
     */
    'fallback_locale' => 'en',

    'locales' => ['en', 'fr'],

    'default_required_all_tr' => env('NOVA_TR_ALL_REQ', true),

    'tiny_mce' => [
        /*
        |--------------------------------------------------------------------------
        | Default Options
        |--------------------------------------------------------------------------
        |
        | Here you can define the options that are passed to all NovaTinyMCE
        | fields by default.
        |
        | Toolbar settings article: https://www.tiny.cloud/blog/tinymce-toolbar/
        | Toolbar controls list: https://www.tiny.cloud/docs/advanced/editor-control-identifiers/#toolbarcontrols
        | Icons identifiers: https://www.tiny.cloud/docs/advanced/editor-icon-identifiers/
        | Content Appearance: https://www.tiny.cloud/docs/configure/content-appearance/#color_cols
        */

        'default_options' => [
            'content_css'   => '/vendor/tinymce/skins/ui/oxide/content.min.css',
            'skin_url'      => '/vendor/tinymce/skins/ui/oxide',
            'path_absolute' => '/',
            'plugins'       => [
                'link lists preview hr anchor pagebreak image wordcount fullscreen directionality paste textpattern'
            ],
            'toolbar'       => 'undo redo | h1 h2 h3 | bold hr fullscreen | forecolor backcolor | alignmentgroup outdent indent | bullist numlist link',
            'toolbar_groups' => [
                'alignmentgroup' => [
                    'icon' => 'align-justify',
                    'tooltip' => 'Alignment',
                    'items' => 'alignleft aligncenter alignright alignjustify'
                ]
            ],
            'relative_urls' => false,
            'menubar'       => false,
            'use_lfm'       => false,
            'lfm_url'       => 'filemanager',
//        'color_map'     => [
//            '000000', 'Black',
//            '808080', 'Gray',
//            'FFFFFF', 'White',
//            'FF0000', 'Red',
//            'FFFF00', 'Yellow',
//            '008000', 'Green',
//            '0000FF', 'Blue'
//        ],
//        'custom_colors' => false
        ],

        /**
         * Light config keeping only bold and links
         */
        'light_options' => [
            'content_css'   => '/vendor/tinymce/skins/ui/oxide/content.min.css',
            'skin_url'      => '/vendor/tinymce/skins/ui/oxide',
            'path_absolute' => '/',
            'plugins'       => [
                'link wordcount fullscreen paste'
            ],
            'toolbar'       => 'undo redo bold fullscreen link',
            'formats' => [
                'italic' => [],
                'underline' => [],
                'h1' => [],
                'h2' => [],
                'h3' => [],
                'h4' => [],
                'h5' => [],
                'h6' => [],
                'address' => [],
            ],
            'relative_urls' => false,
            'menubar'       => false,
            'use_lfm'       => false,
        ],
    ]
];
