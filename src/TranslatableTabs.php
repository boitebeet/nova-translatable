<?php

namespace Boitebeet\NovaTranslatable;

use BoiteBeet\NovaCms\Rules\TranslationRequired;
use Laravel\Nova\Fields\Field;

class TranslatableTabs extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'nova-translatable-tabs';

    /**
     * Create a new field.
     *
     * @param string|null $name
     * @param string|null $attribute
     * @param mixed|null $resolveCallback
     * @return void
     */
    public function __construct($name = null, $attribute = null, $resolveCallback = null)
    {
        parent::__construct("Translatable Locale Tabs", $attribute, $resolveCallback);

        $locales = is_callable(config('nova-translatable.locales', [])) ?
            config('nova-translatable.locales')() :
            config('nova-translatable.locales', []);

        $this->withMeta([
            'locales'       => $locales,
            'currentLocale' => app()->getLocale(),
        ]);

        $this->hideFromIndex();
        $this->fillUsing(fn() => null);
    }

    /**
     * Resolve the given attribute from the given resource.
     *
     * @param mixed $resource
     * @param string $attribute
     * @return mixed
     */
    protected function resolveAttribute($resource, $attribute)
    {
        return null;
    }

    /**
     * Set the locales to display / edit.
     *
     * @param array $locales
     * @return $this
     */
    public function locales(array $locales)
    {
        return $this->withMeta(['locales' => $locales]);
    }

    /**
     * Set the locale to display on detail and form.
     *
     * @param string $locale
     * @return $this
     */
    public function currentLocale($locale)
    {
        return $this->withMeta(['currentLocale' => $locale]);
    }

}
