<?php

namespace Boitebeet\NovaTranslatable\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class TranslatableItemsRules implements Rule
{
    private array $rules;

    private \Illuminate\Contracts\Validation\Validator $validator;

    public function __construct(array $rules)
    {
        $this->rules = $rules;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->validator = Validator::make($value, ["*" => $this->rules]);

        return !$this->validator->fails();
    }

    public function message()
    {
        return collect($this->validator->getMessageBag()->toArray())->first();
    }
}
