<?php

namespace Boitebeet\NovaTranslatable\Rules;

use BoiteBeet\NovaCms\Models\Locale;
use Illuminate\Contracts\Validation\Rule;

class TranslationRequired implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $locales = is_callable(config('nova-translatable.locales', [])) ?
            config('nova-translatable.locales')() :
            config('nova-translatable.locales', []);
        foreach ($locales as $locale){
            if(!isset($value[$locale])){
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.translations.required');
    }
}
