<?php

namespace Boitebeet\NovaTranslatable;

use Boitebeet\NovaTranslatable\Rules\TranslatableItemsRules;
use Boitebeet\NovaTranslatable\Rules\TranslationRequired;
use Illuminate\Contracts\Validation\Rule;
use Laravel\Nova\Fields\Field;
use Str;

class Translatable extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'nova-translatable';

    /**
     * Create a new field.
     *
     * @param string $name
     * @param string|null $attribute
     * @param mixed|null $resolveCallback
     * @return void
     */
    public function __construct($name, $attribute = null, $allTranslationsRequired = null, $resolveCallback = null)
    {
        parent::__construct($name, $attribute, $resolveCallback);

        $locales = is_callable(config('nova-translatable.locales', [])) ?
            config('nova-translatable.locales')() :
            config('nova-translatable.locales', []);

        if ((!is_null($allTranslationsRequired) && $allTranslationsRequired) || (is_null($allTranslationsRequired) && config('nova-translatable.default_required_all_tr'))) {
            $this->requiredAllTranslations();
        }
        $this->withMeta([
            'locales'       => $locales,
            'uuid'          => Str::random(20),
            'indexLocale'   => app()->getLocale(),
            'currentLocale' => app()->getLocale(),
            'singleLine'    => true,
        ]);
    }

    /**
     * Resolve the given attribute from the given resource.
     *
     * @param mixed $resource
     * @param string $attribute
     * @return mixed
     */
    protected function resolveAttribute($resource, $attribute)
    {
        if (method_exists($resource, 'getTranslations')) {
            return $resource->getTranslations($attribute);
        }
        return data_get($resource, $attribute);
    }

    /**
     * Set the locales to display / edit.
     *
     * @param array $locales
     * @return $this
     */
    public function locales(array $locales)
    {
        return $this->withMeta(['locales' => $locales]);
    }

    /**
     * Set the locale to display on index.
     *
     * @param string $locale
     * @return $this
     */
    public function indexLocale($locale)
    {
        return $this->withMeta(['indexLocale' => $locale]);
    }

    /**
     * Set the locale to display on detail and form.
     *
     * @param string $locale
     * @return $this
     */
    public function currentLocale($locale)
    {
        return $this->withMeta(['currentLocale' => $locale]);
    }

    public function setInputType($type = 'singleLine')
    {
        return $this->withMeta([
            'singleLine' => $type === 'singleLine',
            'textArea'   => $type === 'textArea',
            'richText'   => $type === 'richText'
        ]);
    }

    public function singleLine()
    {
        return $this->setInputType('singleLine');
    }

    public function textArea()
    {
        return $this->hideFromIndex()
                    ->setInputType('textArea');
    }

    public function richText()
    {
        return $this->hideFromIndex()
                    ->richTextOptions(null)
                    ->setInputType('richText');
    }

    public function asHtml()
    {
        return $this->withMeta(['asHtml' => true]);
    }

    public function shouldShow($shouldShow = true)
    {
        return $this->withMeta(['shouldShow' => $shouldShow]);
    }

    public function requiredAllTranslations()
    {
        $this->required();
        $this->rules = array_merge($this->rules, ['required', new TranslationRequired]);

        return $this;
    }

    public function parseRulesArguments($rules)
    {
        return ($rules instanceof TranslatableItemsRules) ?
            [$rules] :
            [
                new TranslatableItemsRules(($rules instanceof Rule || is_string($rules)) ? func_get_args() : $rules)
            ];
    }

    /**
     * Set the validation rules for the field.
     *
     * @param callable|array|string $rules
     * @return $this
     */
    public function rules($rules)
    {
        $this->rules = $this->parseRulesArguments($rules);

        return $this;
    }

    /**
     * Set the validation rules for the field.
     *
     * @param callable|array|string $rules
     * @return $this
     */
    public function mergeRules($rules)
    {
        $this->rules = array_merge($this->rules, $this->parseRulesArguments($rules));

        return $this;
    }

    /**
     * Set the creation validation rules for the field.
     *
     * @param  callable|array|string  $rules
     * @return $this
     */
    public function creationRules($rules)
    {
        $this->creationRules = $this->parseRulesArguments($rules);

        return $this;
    }

    /**
     * Set the creation validation rules for the field.
     *
     * @param  callable|array|string  $rules
     * @return $this
     */
    public function updateRules($rules)
    {
        $this->updateRules = $this->parseRulesArguments($rules);

        return $this;
    }

    public function nullable($nullable = true, $values = null)
    {
        $this->required(null);
        $this->rules = array_filter($this->rules, fn($rule) => !($rule instanceof TranslationRequired || $rule === 'required'));
        return parent::nullable($nullable, $values);
    }

    public function richTextOptions($options)
    {
        $options = $options ?? config('nova-translatable.tiny_mce.default_options', []);
        return $this->withMeta(['richTextOptions' => $options]);
    }
}
