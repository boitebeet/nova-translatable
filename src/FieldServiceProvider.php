<?php

namespace Boitebeet\NovaTranslatable;

use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;

class FieldServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadAndPublishConfig();

        $this->loadAndPublishTranslations();

        $this->publishes([
            realpath(__DIR__ . '/../dist/tinymce') => public_path('vendor/tinymce'),
        ], 'public');

        Nova::serving(function (ServingNova $event) {
            Nova::script('nova-translatable', __DIR__.'/../dist/js/field.js');
            Nova::provideToScript(['tinyMceApiKey' => env('TINY_MCE_API_KEY')]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function loadAndPublishConfig()
    {
        $this->publishes([
            __DIR__.'/../config/nova-translatable.php' => config_path('nova-translatable.php'),
        ], 'config');

        $this->mergeConfigFrom(__DIR__.'/../config/nova-translatable.php', 'nova-translatable');
    }

    public function loadAndPublishTranslations()
    {
        if(is_dir(resource_path('lang/vendor/nova-translatable'))){
            $this->loadJsonTranslationsFrom(resource_path('lang/vendor/nova-translatable'));
        } else {
            $this->loadJsonTranslationsFrom(__DIR__.'/../resources/lang');
        }

        $this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/nova-translatable'),
        ], 'nova-translatable-lang');
    }
}
